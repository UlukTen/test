var m = [39, 78, 56, 36, 24, 25, 15, 15, 89, 71];
 
var count = m.length-1;
 
for (var i = 0; i < count; i++) {
     for (var j = 0; j < count-i; j++) {
 
        if (m[j] > m[j+1]) {
           var max = m[j];
           m[j] = m[j+1];
           m[j+1] = max;
        }
     }  
}
 
document.write(m);